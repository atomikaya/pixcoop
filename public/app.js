import * as Canvas from './modules/canvas.js'
import * as Editor from './modules/editor.js'
import * as Player from './modules/player.js'
import * as Router from './modules/router.js'
import * as Store from './modules/store.js'

const state = {
    dataFile: document.querySelector('#data-file'),
    modeSwitch: document.querySelector('.mode-switch'),
    demo: document.querySelector('.demo-link'),
    touchControlled: 'ontouchstart' in window || navigator.maxTouchPoints > 0,
    zoom: 1
}

export const setup = () => {
    let vw = getViewportWidth()
    state.zoom = vw > 900 ? vw > 1200 ? 2 : 1.5 : 1
    
    document.body.classList
        .add('zoom-' + state.zoom.toString().replace('.', '-'))

    Router.setup()
    Store.setup()
    Canvas.warmup(state.zoom)
    Player.setup(state.touchControlled, state.zoom)

    state.dataFile.addEventListener('change', importStory)
    state.modeSwitch.addEventListener('click', startEditMode)
    state.demo.addEventListener('click', loadDemo)
}

const startPlayMode = () => {
    Editor.teardown()
    Canvas.teardown(state.touchControlled)
    Player.setup(state.touchControlled, state.zoom)

    document.body.classList.remove('edit-mode')
    document.body.classList.add('play-mode')

    state.modeSwitch.removeEventListener('click', startPlayMode)
    state.modeSwitch.addEventListener('click', startEditMode)
    state.modeSwitch.textContent = 'Edit'
}

const startEditMode = () => {
    Player.teardown(state.touchControlled)
    Canvas.setup(state.touchControlled)
    Editor.setup(state.zoom)

    document.body.classList.remove('play-mode')
    document.body.classList.add('edit-mode')

    state.modeSwitch.removeEventListener('click', startEditMode)
    state.modeSwitch.addEventListener('click', startPlayMode)
    state.modeSwitch.textContent = 'Play!'
}

const importStory = e => {
    if (!window.File || !window.FileReader || !window.FileList || !window.Blob) {
        return
    }

    const reader = new FileReader()
    if (e.target.files && e.target.files[0]) {           
        reader.onload = e => importText(e.target.result)

        reader.readAsText(e.target.files[0])
    }
}

const importText = text => {
    let data = {}

    try {
        data = JSON.parse(text)
    } catch (e) {
        return
    }

    if (!('title' in data) || !('grid.index' in data) || !('routes' in data)) {
        return
    }

    Store.saveStory(data)
    location.replace('./')
}

const loadDemo = e => {
    e.preventDefault()

    console.log(e.target.href)

    fetch (new Request(e.target.href))
        .then(response => response.text())
        .then(importText)
}

const getViewportWidth = () =>
    Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0)
