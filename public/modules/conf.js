export const conf = {
    width: 30,
    height: 20,
    unit: 12,
    colors: [
        'fff',
        'aaa',
        '000',
        'd78',
        'c23',
        '623',
        'fd6',
        'e83',
        'a62',
        '9c2',
        '482',
        '443',
        'ade',
        '39e',
        '056',
        '223'
    ]
}
