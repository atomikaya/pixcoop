import { inBounds } from './canvas.js'
import { followRoute, getRoute, routeExists } from './router.js'
import { getBlock, loadLink, loadTitle } from './store.js'

const state = {
    title: document.querySelector('.header-title'),
    viewer: document.querySelector('.viewer'),
    reader: document.querySelector('.reader'),
    code: 0,
    zoom: 1
}

export const setup = (touchControlled, zoom) => {
    state.zoom = zoom
    state.title.textContent = document.title = loadTitle() || 'No title yet'

    if (touchControlled) {
        state.viewer.addEventListener('click', updateLink)
        state.reader.addEventListener('click', followLink)
    } else {
        state.viewer.addEventListener('mousemove', updateLink)
        state.viewer.addEventListener('click', followLink)
    }
}

export const teardown = touchControlled => {
    if (touchControlled) {
        state.viewer.removeEventListener('click', updateLink)
        state.reader.removeEventListener('click', followLink)
    } else {
        state.viewer.removeEventListener('mousemove', updateLink)
        state.viewer.removeEventListener('click', followLink)
    }
}

// helpers

const followLink = e => {
    if (state.code === '0' || !state.viewer.classList.contains('viewer-link')) {
        return
    }

    let link = loadLink(getRoute(), state.code)

    if (routeExists(link.name)) {
        followRoute(link.name)

        state.code = '0'
        state.reader.innerHTML = ''
        state.viewer.classList.remove('viewer-link')
        state.reader.classList.remove('reader-link')
    }

}

const renderHint = (hint, hasLink) => {
    if (hint === '' && !hasLink) {
        return state.reader.innerHTML = ''
    }

    state.reader.innerHTML = hint.split("\n")
        .reduce(
            (acc, quote) =>
                acc + `<blockquote class="reader-hint">${quote}</blockquote>`,
            ''
        )
}

const updateLink = e => {
    let x = Math.floor(e.offsetX / state.zoom)
    let y = Math.floor(e.offsetY / state.zoom)

    if (!inBounds(x, y)) {
        return
    }

    let block = getBlock(x, y)

    if (block.code === state.code) {
        return
    }

    state.code = block.code

    let link = loadLink(getRoute(), state.code)

    renderHint(link.hint, link.name !== '')

    if (link.name !== '') {
        state.viewer.classList.add('viewer-link')
        state.reader.classList.add('reader-link')
    } else {
        state.viewer.classList.remove('viewer-link')
        state.reader.classList.remove('reader-link')
    }
}
