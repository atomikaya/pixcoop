const state = {
    picker: document.querySelector('.tools-page-picker'),
    nameData: document.querySelector('#page-names')
}

export const setup = () => {
    if (!localStorage.getItem('routes')) {
        saveRoutes(['index'])
    }

    renderRoutes(state.picker, true)
    renderRoutes(state.nameData)
    
    state.picker.addEventListener('change', redirect)
    window.addEventListener('hashchange', updateRoutes)
}

// exports

export const clearRoute = r =>
    saveRoutes((loadRoutes() || []).filter(route => route !== r))

export const clearRoutes = () => localStorage.removeItem('routes')

export const followRoute = route =>
    location.hash = route === 'index' ? '' : '#' + route

export const getRoute = () =>
    location.hash.slice(1) || 'index'

export const renderRoutes = (target, showSelected) => {
    target.innerHTML = ''

    loadRoutes().sort().forEach(appendRoute(target, showSelected))
}

export const routeExists = name => (loadRoutes() || []).includes(name)

// helpers

const appendRoute = (target, showSelected) => route =>
    target.innerHTML +=
        `<option value="${route}"${showSelected && route === getRoute() ? ' selected' : ''}>
            ${route}
        </option>`

const loadRoutes = () => JSON.parse(localStorage.getItem('routes'))

const redirect = e => followRoute(e.target.value)

const saveRoutes = routes =>
    localStorage.setItem('routes', JSON.stringify(routes))

const updateRoutes = () => {
    let routes = loadRoutes() || []

    if (!routes.includes(getRoute())) {
        routes.push(getRoute())
    }

    saveRoutes(routes)
    renderRoutes(state.picker, true)
}
