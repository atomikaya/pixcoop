import { refresh } from './canvas.js'
import { conf } from './conf.js'
import { clearRoute, clearRoutes, followRoute, getRoute } from './router.js'
import { copyBlocks, loadLink, loadStory, loadTitle, pasteBlocks, removeBlocks, sanitize, saveHint, saveName, saveStory, saveTitle } from './store.js'

const state = {
    title: document.querySelector('.header-title-input'),
    tabs: document.querySelectorAll('.tab'),
    colorbox: document.querySelector('.colorbox'),
    linkbox: document.querySelector('.linkbox'),
    details: document.querySelector('.link-details'),
    linkName: document.querySelector('.link-name'),
    linkHint: document.querySelector('.link-hint'),
    pen: conf.colors[0],
    hook: '0',
    pageCopy: null,
    tool: {
        clearPage: document.querySelector('.tools-clear-page'),
        copyPage: document.querySelector('.tools-copy-page'),
        createPage: document.querySelector('.tools-create-page'),
        deletePage: document.querySelector('.tools-delete-page'),
        deleteAll: document.querySelector('.tools-delete-all'),
        import: document.querySelector('.tools-import'),
        export: document.querySelector('.tools-export')
    }
}

export const setup = () => {
    state.colorbox.innerHTML = conf.colors
        .reduce((list, color) => list + renderPen(color), '')
    
    state.linkbox.innerHTML = [... conf.colors.keys()]
        .map(i => i.toString(16))
        .reduce((list, code) => list + renderHook(code), '')
    state.title.value = loadTitle()

    state.title.addEventListener('input', saveTitle)
    state.tabs.forEach(setupTab)
    state.colorbox.querySelectorAll('.palette-item').forEach(setupPen)
    state.linkbox.querySelectorAll('.palette-item').forEach(setupHook)
    state.tool.clearPage.addEventListener('click', clearPage)
    state.tool.copyPage.addEventListener('click', copyPage)
    state.tool.createPage.addEventListener('click', createPage)
    state.tool.deletePage.addEventListener('click', deletePage)
    state.tool.deleteAll.addEventListener('click', deleteAll)
    state.tool.export.addEventListener('click', exportStory)
    state.linkName.addEventListener('input', saveLinkName)
    state.linkHint.addEventListener('input', saveLinkHint)
    window.addEventListener('hashchange', updateEditor)
    
    updateEditor()
}

const setupHook = hook =>
    hook.addEventListener('click', switchHook)

const setupPen = pen =>
    pen.addEventListener('click', switchPen)

const setupTab = tab => {
    tab.disabled = tab.classList.contains('colors-tab')

    tab.addEventListener('click', switchTab)
}
export const teardown = () => {
    state.title.removeEventListener('input', saveTitle)
    state.tabs.forEach(teardownTab)
    state.colorbox.querySelectorAll('.palette-item').forEach(teardownPen)
    state.linkbox.querySelectorAll('.palette-item').forEach(teardownHook)
    state.tool.clearPage.removeEventListener('click', clearPage)
    state.tool.createPage.removeEventListener('click', createPage)
    state.tool.deletePage.removeEventListener('click', deletePage)
    state.tool.deleteAll.removeEventListener('click', deleteAll)
    state.tool.export.removeEventListener('click', exportStory)
    state.linkName.removeEventListener('input',saveLinkName)
    state.linkHint.removeEventListener('input', saveLinkHint)
    window.removeEventListener('hashchange', updatePageDeletionAvailability)
}

const teardownHook = hook =>
    hook.removeEventListener('click', switchHook)

const teardownPen = pen =>
    pen.removeEventListener('click', switchPen)

const teardownTab = tab => {
    tab.disabled = tab.classList.contains('colors-tab')

    tab.removeEventListener('click', switchTab)
}

// helpers

const clearPage = () => {
    removeBlocks()
    refresh()
}

const copyPage = e => {
    if (e.target.classList.contains('copied')) {
        pasteBlocks()
        refresh()
        e.target.classList.remove('copied')
        e.target.textContent = 'Copy'
    } else {
        copyBlocks()
        e.target.classList.add('copied')
        e.target.textContent = 'Paste'
    }
}

const createPage = () => {
    if (state.hook === '0') {
        return
    }

    let link = loadLink(getRoute(), state.hook)

    followRoute(link.name)
}

const deletePage = () => {
    if (getRoute() === 'index') {
        return
    }

    removeBlocks()
    clearRoute(getRoute())

    location.hash = ''
}

const deleteAll = () => {
    localStorage.clear()
    clearRoutes()

    location.replace('./')
}

const exportStory = () => {
    state.tool.export.setAttribute('download', sanitize(loadTitle()) + '.json')
    state.tool.export.setAttribute(
            'href',
            'data:application/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(loadStory()))
        )
}

const renderHook = code =>
    `<li>
        <button
            class="palette-item${code === state.hook ? ' palette-selected' : ''}"
            data-link="${code}"
        >${code === '0' ? '' : code}</button>
    </li>`

const renderPen = color =>
    `<li>
        <button
            class="palette-item${color === state.pen ? ' palette-selected' : ''}"
            data-color="${color}"
            style="background: #${color};"
        ></button>
    </li>`

const saveLinkName = e => saveName(getRoute(), state.hook, e.target)

const saveLinkHint = e => saveHint(getRoute(), state.hook, e.target)

const switchHook = e => {
    e.preventDefault()
    state.hook = e.target.textContent || '0'

    let selected = state.linkbox.querySelector('.palette-selected')
    let link = loadLink(getRoute(), state.hook)

    state.linkName.value = link.name
    state.linkHint.value = link.hint
    
    if (selected) {
        state.linkbox
            .querySelector('.palette-selected')
            .classList.remove('palette-selected')
    }

    e.target.classList.add('palette-selected')
    updateLinkDetailsVisibility()
    document.dispatchEvent(new CustomEvent('hookchange', { detail: state.hook }))
}

const switchPen = e => {
    e.preventDefault()
    state.pen = e.target.getAttribute('data-color')

    state.colorbox.querySelector('.palette-selected').classList.remove('palette-selected')
    e.target.classList.add('palette-selected')
    document.dispatchEvent(new CustomEvent('penchange', { detail: state.pen }))
}

const switchTab = e => {
    state.tabs.forEach(tab => tab.disabled = tab === e.target)

    document.dispatchEvent(new CustomEvent('linkmodechange', {
        detail: e.target.getAttribute('data-linkmode') === 'true'
    }))
}

const updateEditor = () => {
    updateLinkDetailsVisibility()
    updatePageDeletionAvailability()
}

const updateLinkDetailsVisibility = () => {
    if (state.hook === '0') {
        state.details.classList.add('link-empty')
    } else {
        state.details.classList.remove('link-empty')
    }

    let link = loadLink(getRoute(), state.hook)

    state.linkName.value = link.name
    state.linkHint.value = link.hint
}

const updatePageDeletionAvailability = () =>
    state.tool.deletePage.disabled = getRoute() === 'index'
