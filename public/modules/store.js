import { conf } from './conf.js'
import { getRoute } from './router.js'

const state = {
    blocksCopy: null,
    linksCopy: null
}

export const setup = () => {
    if (!hasBlocks()) {
        setupBlocks()
    }
    
    document.addEventListener('blockupdate', updateBlock)
}

// exports

export const copyBlocks = () => {
    state.blocksCopy = loadBlocks()
    state.linksCopy = loadLinks()
}

export const getBlock = (x, y) => loadBlocks()[getKey(x, y)]

export const getBlockList = () => {
    if (!hasBlocks()) {
        setup()
    }

    return Object.values(loadBlocks())
}

export const loadLink = (route, code) =>
    JSON.parse(localStorage.getItem('link.' + route + '.' + code))
        || { name: '', hint: '' }

export const loadStory = () => {
    let data = {}

    for (let i = 0; i < localStorage.length; i++) {
        let key = localStorage.key(i)
        let item = localStorage.getItem(key)

        try {
            data[key] = JSON.parse(item)
        } catch (e) {
            data[key] = item
        }
    }

    return data
}

export const loadTitle = () => localStorage.getItem('title')

export const pasteBlocks = () => {
    if (state.blocksCopy) {
        saveBlocks(state.blocksCopy)
    }

    if (state.linksCopy) {
        const route = getRoute()
        let links = {}

        Object.keys(state.linksCopy).forEach(key =>
            links[key.replace(/^link\.[\w-]+/, 'link.' + route)] = state.linksCopy[key]
        )

        saveLinks(links)
    }
}

export const removeBlocks = () => localStorage.removeItem('grid.' + getRoute())

export const saveHint = (route, code, hint) => {
    let link = loadLink(route, code)

    saveLink(route, code, link.name, hint.value)
}

export const saveName = (route, code, name) => {

    let link = loadLink(route, code)

    name.value = sanitize(name.value)

    saveLink(route, code, name.value, link.hint)
}

export const sanitize = text =>
    text
        .replace(/[\s_/:]+/g, '-')
        .replace(/-+/g, '-')
        .replace(/[^\w-]+/g, '')

export const saveStory = data => {
    localStorage.clear()
    
    Object.keys(data).forEach(key =>
        localStorage.setItem(
            key,
            typeof(data[key]) === 'object'
                ? JSON.stringify(data[key])
                : data[key]
        )
    )
}

export const saveTitle = e => localStorage.setItem('title', e.target.value)

export const snap = pos => pos - pos % conf.unit

// helpers

const getIndex = pos => Math.floor(pos / conf.unit)

const getKey = (x, y) => getIndex(x) + 'x' +  getIndex(y)

const hasBlocks = () => true && localStorage.getItem('grid.' + getRoute())

const loadBlocks = () => JSON.parse(localStorage.getItem('grid.' + getRoute()))

const loadLinks = () => {
    let links = {}

    for (let i = 0; i < localStorage.length; i++) {
        let key = localStorage.key(i)
        let item = localStorage.getItem(key)
        
        if (key.startsWith('link.' + getRoute() + '.')) {
            links[key] = item
        }
    }

    return links
}

const saveBlocks = blocks =>
    localStorage.setItem('grid.' + getRoute(), JSON.stringify(blocks))

const saveLink = (route, code, name, hint) =>
    localStorage.setItem(
        'link.' + route + '.' + code,
        JSON.stringify({ name, hint })
    )

const saveLinks = links =>
    Object.keys(links).forEach(key => localStorage.setItem(key, links[key]))

const setupBlocks = () => {
    let blocks = {}

    for (let r = 0; r < conf.height; r++) {
        for (let c = 0; c < conf.width; c++) {
            blocks[c + 'x' + r] = { c, r, color: '000', code: '0' }
        }
    }

    saveBlocks(blocks)
}

const updateBlock = e => {
    let blocks = loadBlocks()
    let key = getKey(e.detail.x, e.detail.y)

    blocks[key].color = e.detail.color
    blocks[key].code = e.detail.code

    saveBlocks(blocks)
}
