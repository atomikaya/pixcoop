import { conf } from './conf.js'
import { getBlock, getBlockList, snap } from './store.js'

const state = {
    canvas: document.querySelector('.viewer'),
    ctx: null,
    width: 0,
    height: 0,
    drawing: false,
    linkMode: false,
    color: conf.colors[0],
    code: '0',
    zoom: 1
}

export const warmup = zoom => {
    state.zoom = zoom
    state.width = conf.width * conf.unit * state.zoom,
    state.height = conf.height * conf.unit * state.zoom,
    state.ctx = state.canvas.getContext('2d', { alpha: false })
    state.canvas.width = state.width
    state.canvas.height = state.height

    refresh()

    window.addEventListener('hashchange', refresh)
}

export const setup = touchControlled => {
    if (touchControlled) {
        state.canvas.addEventListener('touchmove', handleMove)
        state.canvas.addEventListener('touchstart', startLine)
        state.canvas.addEventListener('contextmenu', disableEvent)
        document.addEventListener('touchend', endLine)
    } else {
        state.canvas.addEventListener('mousemove', handleMove)
        state.canvas.addEventListener('mousedown', startLine)
        document.addEventListener('mouseup', endLine)
    }
    document.addEventListener('penchange', changeColor)
    document.addEventListener('hookchange', changeCode)
    document.addEventListener('linkmodechange', changeLinkMode)
}

export const teardown = touchControlled => {
    state.linkMode = false

    refresh()

    if (touchControlled) {
        state.canvas.removeEventListener('touchmove', handleMove)
        state.canvas.removeEventListener('touchstart', startLine)
        state.canvas.removeEventListener('contextmenu', disableEvent)
        document.removeEventListener('touchend', endLine)
    } else {
        state.canvas.removeEventListener('mousemove', handleMove)
        state.canvas.removeEventListener('mousedown', startLine)
        document.removeEventListener('mouseup', endLine)
    }
    document.removeEventListener('penchange', changeColor)
    document.removeEventListener('hookchange', changeCode)
    document.removeEventListener('linkmodechange', changeLinkMode)
}

// exports

export const inBounds = (x, y) =>
    x > 0 && x < state.width && y > 0 && y < state.height

export const refresh = list =>
    getBlockList().forEach(
        ({ c, r }) => draw(c * conf.unit, r * conf.unit, true)
    )

// helpers

const changeCode = e => state.code = e.detail

const changeColor = e => state.color = e.detail

const changeLinkMode = e => {
    state.linkMode = e.detail

    refresh()
}

const contrastColor = color =>
    parseInt(color, 16) > 2047 ? '000' : 'fff'

const draw = (x, y, refresh) => {
    console.log(x, y)
    let block = getBlock(x, y)
    let color = state.linkMode || refresh ? block.color : state.color
    let code = !state.linkMode || refresh ? block.code : state.code

    state.ctx.fillStyle = '#' + color

    state.ctx.fillRect(
        snap(x) * state.zoom,
        snap(y) * state.zoom,
        conf.unit * state.zoom,
        conf.unit * state.zoom
    )
    
    if (state.linkMode && code !== '0') {
        state.ctx.fillStyle = '#' + contrastColor(color)
    
        state.ctx.fillText(
            code,
            (snap(x) + 3) * state.zoom,
            (snap(y) + conf.unit - 2) * state.zoom
        )
    }

    if (!refresh) {
        document.dispatchEvent(
            new CustomEvent('blockupdate', { detail: { x, y, color, code }})
        )
    }
}

const getOffset = e => {
    const rect = e.target.getBoundingClientRect()
    let x = Math.floor((e.targetTouches ? e.targetTouches[0].clientX - rect.x : e.offsetX) / state.zoom)
    let y = Math.floor((e.targetTouches ? e.targetTouches[0].clientY - rect.y : e.offsetY) / state.zoom)

    return { x, y }
}

const endLine = e => {
    state.drawing = false
}

const handleMove = e => {
    const offset = getOffset(e)

    if (state.drawing && inBounds(offset.x, offset.y)) {
        draw(offset.x, offset.y)
    }
}

const disableEvent = e => e.preventDefault()

const startLine = e => {
    const offset = getOffset(e)
    state.drawing = true

    draw(offset.x, offset.y)
}
